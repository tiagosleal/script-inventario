# script-inventario

Coleta inventário de Hardware e Software da máquina e salva em formato HTML.

# Get Started

Salva o inventário no diretório atual
```batch
C:\Users\Administrator>cscript.exe //NoLogo script-inventario.vbs
```
Salva o inventário no diretório d:\
```batch
C:\Users\Administrator>cscript.exe //NoLogo script-inventario.vbs d:\
```

O `hostname da máquina` é o nome do arquivo de inventário. Ex.: 
```servidor-dns.html```